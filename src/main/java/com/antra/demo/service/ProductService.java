package com.antra.demo.service;

import java.util.List;

import com.antra.demo.models.Product;

public interface ProductService {
	
	List<Product> findProducts();
	
	Product  findProductById(Integer id);
	
	boolean  addProduct(Product product);
	
	boolean  updateProduct(Product product);
	
	boolean  deleteProduct(Integer pid);
	

}
