package com.antra.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.demo.entities.ProductEntity;
import com.antra.demo.exception.ProductNotFoundException;
import com.antra.demo.models.Product;
import com.antra.demo.repository.ProductEntityDAO;
import com.antra.demo.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductEntityDAO  dao;

	@Override
	public List<Product> findProducts() {
		
		List<ProductEntity>  lstProductEntities = dao.findAll();
		
		List<Product>  lstProducts = new ArrayList<>();
		
		lstProductEntities.forEach( entity -> {
			Product product = new Product();
			BeanUtils.copyProperties(entity, product);
			lstProducts.add(product);
		});
		return lstProducts;
		
	}

	@Override
	public Product findProductById(Integer id) {
		
		Optional<ProductEntity>  opt = dao.findById(id);
		if(opt.isPresent()) {
			ProductEntity entity = opt.get();
			Product product = new Product();
			BeanUtils.copyProperties(entity, product);
			return product;
		}
		else {
			return null;
		}
	}

	@Override
	public boolean addProduct(Product product) {
		ProductEntity  entity = new ProductEntity();
		BeanUtils.copyProperties(product, entity);
		if(dao.existsById(entity.getProductId())) {
			return false;
		}
		else {
			dao.save(entity);
			return true;
		}
	}

	@Override
	public boolean updateProduct(Product product) {
		ProductEntity  entity = new ProductEntity();
		BeanUtils.copyProperties(product, entity);
		
		if(dao.existsById(entity.getProductId())) {
			dao.saveAndFlush(entity);
			return true;
		}
		else {
			throw new ProductNotFoundException("No product found with id " +product.getProductId());
		}
	}

	@Override
	public boolean deleteProduct(Integer pid) {
		if(dao.existsById(pid)) {
			dao.deleteById(pid);
			return true;
		}
		else {
			return false;
		}
	}

}
