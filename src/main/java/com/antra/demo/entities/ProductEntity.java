package com.antra.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table( name = "PRODUCTS_TAB")
public class ProductEntity {
	@Id
	private Integer productId;
	
	@Column(length=25)
	private String productName;
	
	private Double price;
	

}
