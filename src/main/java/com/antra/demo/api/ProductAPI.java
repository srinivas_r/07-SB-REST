package com.antra.demo.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.antra.demo.models.Product;
import com.antra.demo.service.ProductService;

@RestController
public class ProductAPI {
	@Autowired
	ProductService  service;
	
	@GetMapping(value = "/products")
	public ResponseEntity<?> getProducts() {
		List<Product>  lstProducts = service.findProducts();
		ResponseEntity<List<Product>> re = new ResponseEntity<>(lstProducts, HttpStatus.OK);
		return re;
	}
	
	@GetMapping(value = "/products/{id}")
	public ResponseEntity<?> getProductById(@PathVariable(value = "id") Integer pid) {
		Product product = service.findProductById(pid);
		ResponseEntity<Product>  re = new ResponseEntity<>(product, HttpStatus.OK);
		return re;
		
	}
	
	@PostMapping(value = "/products/add")
	public ResponseEntity<?> addProduct(@RequestBody Product product) {
		boolean flag = service.addProduct(product);
		if(flag == true) {
			return new ResponseEntity<String>("Product is added", HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<String>("Product already exist", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PutMapping(value = "/products/update")
	public ResponseEntity<?> updateProduct(@RequestBody Product product) {
		
		service.updateProduct(product);
		
		return new ResponseEntity<String>("Product is updated", HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value = "/products/delete")
	public ResponseEntity<?>  deleteProduct(@RequestParam Integer productId) {
		boolean flag = service.deleteProduct(productId);
		if(flag == true) {
			return new ResponseEntity<String>("Product is deleted", HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<String>("Product doesn't exist", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
}
