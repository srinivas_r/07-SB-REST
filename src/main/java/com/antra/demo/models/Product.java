package com.antra.demo.models;

import lombok.Data;

@Data
public class Product {
	private Integer productId;
	private String productName;
	private Double price;
	

}
