package com.antra.demo.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.antra.demo.exception.ProductNotFoundException;

@RestControllerAdvice
public class MyRestControllerAdvice {
	
	@ExceptionHandler(ProductNotFoundException.class)
	public ErrorMessage  exHandler1(ProductNotFoundException ex) {
		
		int code = HttpStatus.BAD_REQUEST.value();
		String msg = ex.getMessage();
		
		ErrorMessage  err = new ErrorMessage(code, msg);	
		return err;
	}

}
