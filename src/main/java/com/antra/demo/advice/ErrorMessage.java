package com.antra.demo.advice;

public class ErrorMessage {
	private int errCode;
	private String errMessage;
	
	public ErrorMessage(int errCode, String errMessage) {
		super();
		this.errCode = errCode;
		this.errMessage = errMessage;
	}

	public int getErrCode() {
		return errCode;
	}

	public String getErrMessage() {
		return errMessage;
	}
	
	

}
