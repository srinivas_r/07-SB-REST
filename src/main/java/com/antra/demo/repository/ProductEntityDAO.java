package com.antra.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antra.demo.entities.ProductEntity;

@Repository
public interface ProductEntityDAO extends JpaRepository<ProductEntity, Integer> {

}
